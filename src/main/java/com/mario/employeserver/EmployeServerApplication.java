package com.mario.employeserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeServerApplication.class, args);
	}

}
