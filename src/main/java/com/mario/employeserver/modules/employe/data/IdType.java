package com.mario.employeserver.modules.employe.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
* This class represent to id_table db table.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

@Entity	
@Table(name = "id_type")
public class IdType implements Serializable{
    
    /**
     *
     */
    private static final long serialVersionUID = 5443344958734167031L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)		
    @Column(name="id")		
    Long id;		
    
    @Column(name="value")		
    String value;		
    
    @Column(name="description")		
    String description;

    @JsonIgnore
    @OneToMany(targetEntity=Employe.class, mappedBy="idType", fetch = FetchType.LAZY)    
    private List<Employe> employes = new ArrayList<>();

    public IdType() {
    }

    public IdType(Long id, String value, String description) {
        this.id = id;
        this.value = value;
        this.description = description;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IdType id(Long id) {
        this.id = id;
        return this;
    }

    public IdType value(String value) {
        this.value = value;
        return this;
    }

    public IdType description(String description) {
        this.description = description;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", value='" + getValue() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }

}
