package com.mario.employeserver.modules.employe.controllers;

import com.mario.employeserver.modules.employe.data.Area;
import com.mario.employeserver.modules.employe.services.AreaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
* This class handler interaction beetwen de user and system
* providen web service to handler data of area table.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

@Controller 
@RequestMapping(path="/area")
public class AreaController {
    
    @Autowired
    AreaService areaService;

    /**
     * This method retiable records to country table on Json data 
     * @return Json, json list records of country table.
   */
    @GetMapping(path="/all")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody Iterable<Area> getAllAreas() {
        return areaService.getAllAreas();
    }
}
