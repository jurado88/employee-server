package com.mario.employeserver.modules.employe.services;

import com.mario.employeserver.modules.employe.data.Area;

/**
* This interface defined to behavior transations on area object.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

public interface AreaService {
 
    Iterable<Area> getAllAreas();
}
