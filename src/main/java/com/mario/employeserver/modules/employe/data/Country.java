package com.mario.employeserver.modules.employe.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
* This class represent to country db table.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

@Entity	
@Table(name = "country")
public class Country implements Serializable{
 
    /**
     *
     */
    private static final long serialVersionUID = -4027904378399149812L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)		
    @Column(name="id")		
    Long id;		
    
    @Column(name="name")		
    String name;		

    @JsonIgnore
    @OneToMany(targetEntity=Employe.class, mappedBy="country", fetch = FetchType.LAZY)    
    private List<Employe> employes = new ArrayList<>();

    public Country() {
    }

    public Country(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country id(Long id) {
        this.id = id;
        return this;
    }

    public Country name(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
    
}
