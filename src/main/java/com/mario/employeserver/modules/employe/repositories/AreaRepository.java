package com.mario.employeserver.modules.employe.repositories;

import com.mario.employeserver.modules.employe.data.Area;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
* This interface handler CRUD transactions on database for area table.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

@Repository
public interface AreaRepository extends CrudRepository<Area,Long>{
    
}