package com.mario.employeserver.modules.employe.repositories;

import java.util.List;

import com.mario.employeserver.modules.employe.data.Employe;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
* This interface handler CRUD transactions on database for employe table.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

@Repository
public interface EmployeRepository extends CrudRepository<Employe,Long>{

    Page<Employe> findAll(Pageable pageable);
    List<Employe> findByIdentification(String identification);
    @Query(value = "select e.email from employe e where e.email like CONCAT(:searchWord,'%') and e.country = :idCountry order by creation_date desc limit 1", nativeQuery = true)
    public String findLastEmail(@Param("searchWord") String searchWord, @Param("idCountry") Long idCountry);
    @Query(value = "from Employe e where LOWER(e.email) like LOWER(CONCAT('%',:criterion,'%')) or LOWER(e.firstName) like LOWER(CONCAT('%',:criterion,'%')) or LOWER(e.otherName) like LOWER(CONCAT('%',:criterion,'%')) or LOWER(e.firstSurname) like LOWER(CONCAT('%',:criterion,'%')) or LOWER(e.secondSurname) like LOWER(CONCAT('%',:criterion,'%')) or LOWER(e.area.value) like LOWER(CONCAT('%',:criterion,'%')) or LOWER(e.country.name) like LOWER(CONCAT('%',:criterion,'%')) or LOWER(e.idType.value) like LOWER(CONCAT('%',:criterion,'%')) or LOWER(e.identification) like LOWER(CONCAT('%',:criterion,'%')) or LOWER(e.creationDate) like LOWER(CONCAT('%',:criterion,'%'))")
    Page<Employe> findByCriterion(@Param("criterion") String criterion, Pageable pageable);
    
}
