package com.mario.employeserver.modules.employe.services;

import com.mario.employeserver.modules.employe.data.IdType;
import com.mario.employeserver.modules.employe.repositories.IdTypeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* This class implement IdType service and defined
* some method to handler business logic for id_type
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/
 
@Service
public class IdTypeServiceImp implements IdTypeService {

    @Autowired
    IdTypeRepository idTypeRepository;

    @Override
    public Iterable<IdType> getAllIdsType() {
        return idTypeRepository.findAll();
    }
    
}
