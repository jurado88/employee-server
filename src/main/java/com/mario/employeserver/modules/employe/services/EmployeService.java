package com.mario.employeserver.modules.employe.services;

import java.util.List;

import com.mario.employeserver.modules.employe.data.Employe;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeService {
    
    Employe insertEmploye(Employe employe);
    Employe updateEmploye(Employe employe);
    void deleteEmployee(Employe employe);
    Iterable<Employe> getAllEmploye();
    Page<Employe> findAll(Pageable pageable);
    List<Employe> findByIdentification(String identification);

    Page<Employe> findAll(Pageable pageable, String criterion);

}
