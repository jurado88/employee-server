package com.mario.employeserver.modules.employe.repositories;

import com.mario.employeserver.modules.employe.data.IdType;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
* This interface handler CRUD transactions on database for id_type table.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

@Repository
public interface IdTypeRepository extends CrudRepository<IdType,Long>{
    
}