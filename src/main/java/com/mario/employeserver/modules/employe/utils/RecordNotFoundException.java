package com.mario.employeserver.modules.employe.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This class handler response to exception not found
 *
 * @author Mario Jurado
 * @version 1.0
 * @since 2020-12-04
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class RecordNotFoundException extends RuntimeException 
{
    
    private static final long serialVersionUID = 6114918982401246273L;

    public RecordNotFoundException(String exception) {
        super(exception);
    }
}