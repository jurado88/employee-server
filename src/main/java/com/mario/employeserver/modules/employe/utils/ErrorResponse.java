package com.mario.employeserver.modules.employe.utils;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
 
/**
 * This class is a model DTO to response exception
 *
 * @author Mario Jurado
 * @version 1.0
 * @since 2020-12-04
 */
@XmlRootElement(name = "error")
public class ErrorResponse 
{
    public ErrorResponse(String message, List<String> details) {
        super();
        this.message = message;
        this.details = details;
    }
 
    //General error message about nature of error
    private String message;
 
    //Specific errors in API request processing
    private List<String> details;
 
    //Getter and setters

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getDetails() {
        return this.details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }
}
