package com.mario.employeserver.modules.employe.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;

/**
* This class represent to employe db table.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

@Entity	
@Table(name = "employe")
public class Employe implements Serializable{

    private static final long serialVersionUID = 5832193739256695305L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)		
    @Column(name="id")		
    Long id;		
    
    @Size(min = 1, max = 20, message 
      = "FirstName must be between 1 and 20 characters")
    // @Pattern(regexp = ".*[A-Z].*", message = "FirstName only support uppercases letters")	
    @Column(name="first_name")	
    String firstName;	
    
    @Size(min = 1, max = 50, message 
      = "FirstName must be between 1 and 50 characters")
    // @Pattern(regexp = "^[A-Z\s]")	
    @Column(name="other_name")	
    String otherName;	
    
    @Size(min = 1, max = 20, message 
      = "FirstName must be between 1 and 20 characters")
    // @Pattern(regexp = "^[A-Z\s]")	
    @Column(name="first_surname")		
    String firstSurname;

    @Size(min = 1, max = 20, message 
      = "FirstName must be between 1 and 20 characters")
    // @Pattern(regexp = "^[A-Z\s]")	
    @Column(name="second_surname")		
    String secondSurname;

    @Size(min = 1, max = 20, message 
      = "FirstName must be between 1 and 20 characters")
    // @Pattern(regexp = "^[a-zA-Z0-9\s]")	
    @Column(name="identification")		
    String identification;

    // @Max(300)
    // @NotEmpty(message = "Email is mandatory.")
    // @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", message = "Email be valid")
    @Column(name="email")		
    String email;

    // @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", timezone = JsonFormat.DEFAULT_TIMEZONE)
    // @Column(name = "start_date")
    @Column(name = "start_date")
    @DateTimeFormat (pattern = "yyyy/mm/dd")
    @JsonFormat (pattern = "yyyy/mm/dd")
    @Temporal(TemporalType.TIMESTAMP)
    Date startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", timezone = JsonFormat.DEFAULT_TIMEZONE)
    @Column(name = "creation_date")
    Date creationDate;

    @Column(name = "enabled")
    Boolean enabled;    

    @ManyToOne
    @JoinColumn(name="area")
    private Area area;

    @ManyToOne
    @JoinColumn(name="id_type")
    private IdType idType;
    
    @ManyToOne
    @JoinColumn(name="country")
    private Country country;

    public Employe() {
    }

    public Employe(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtherName() {
        return this.otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getFirstSurname() {
        return this.firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return this.secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getIdentification() {
        return this.identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean isEnabled() {
        return this.enabled;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Area getArea() {
        return this.area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public IdType getIdType() {
        return this.idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }


    public Employe id(Long id) {
        this.id = id;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            "}";
    }
    
}
