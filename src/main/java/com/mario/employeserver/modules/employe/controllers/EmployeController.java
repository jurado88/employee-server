package com.mario.employeserver.modules.employe.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.mario.employeserver.modules.employe.data.Employe;
import com.mario.employeserver.modules.employe.services.EmployeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PutMapping;


/**
* This class handler interaction beetwen de user and system
* providen web service to handler data of employe table.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

@Controller 
@RequestMapping(path="/employe")
public class EmployeController {
    
    @Autowired
    EmployeService employeService;

    /**
     * This method allow create a new employe 
     * @param Employe, employee to insert 
     * @return Empoye creared or exception validarion.
    */
    @PostMapping(path="/add") 
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Employe> addNewEmploye(@Valid @RequestBody Employe employe) {
        return new ResponseEntity<>(employeService.insertEmploye(employe), HttpStatus.OK);
    }

    /**
     * This method allow delete an employe 
     * @param Employe, employee to delete 
     * @return ResponseEntity 204 if action done, others in case fail
    */
    @DeleteMapping(path = "/delete")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Employe> deleteEmploye(@RequestBody Employe employe) {
        employeService.deleteEmployee(employe);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * This method retiable records to country table on Json data 
     * @return Json, json list records of country table.
    */
    @GetMapping(path="/all")
    public @ResponseBody Iterable<Employe> getAllEmployes() {
        return employeService.getAllEmploye();
    }

    /**
     * This method allow update an employe 
     * @param Employe, employee to update 
     * @return ResponseEntity<Employe> 200 if action done, others http code in case fail
    */
    @PutMapping(value="/update")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Employe> updateEmployee(@Valid @RequestBody Employe employe) {
        return new ResponseEntity<>(employeService.updateEmploye(employe), HttpStatus.OK);
    }
    
    /**
     * This method find a employe by identification
     * @param id, field to search employe
     * @return ResponseEntity<Employe> if find a employe return http 200 with the employee find
    */
    @GetMapping(path="/identification")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Employe> getEmployeById(@RequestParam String id) {
        List<Employe> employes = employeService.findByIdentification(id);
        if(!employes.isEmpty()){
            return new ResponseEntity<>(employes.get(0),HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    /**
     * This method allow get employes by a criterion 
     * @return ResponseEntity 200 if action done with list of employes, others in case fail
    */
    @GetMapping("/allPageable")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Map<String, Object>> getAllEmployeesPageable(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "") String criterion
        ) {

        try {
        List<Employe> employees = new ArrayList<Employe>();
        Pageable paging = PageRequest.of(page, size, Sort.by("id").descending());
        
        Page<Employe> pageTuts;
        if(criterion.trim().equals("")){
            pageTuts = employeService.findAll(paging);
        }else {
            pageTuts = employeService.findAll(paging,criterion);
        }
        employees = pageTuts.getContent();

        Map<String, Object> response = new HashMap<>();
        response.put("employees", employees);
        response.put("currentPage", pageTuts.getNumber());
        response.put("totalItems", pageTuts.getTotalElements());
        response.put("totalPages", pageTuts.getTotalPages());

        return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
