package com.mario.employeserver.modules.employe.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
* This class represent to area db table.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

@Entity	
@Table(name = "area")
public class Area implements Serializable{
    
    /**
     *
     */
    private static final long serialVersionUID = -1030556003166447699L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)		
    @Column(name="id")		
    Long id;		
    
    @Column(name="value")		
    String value;		
    
    @Column(name="description")		
    String description;

    @JsonIgnore
    @OneToMany(targetEntity=Employe.class, mappedBy="area", fetch = FetchType.LAZY)    
    private List<Employe> employes = new ArrayList<>();

    public Area() {
    }

    public Area(Long id, String value, String description) {
        this.id = id;
        this.value = value;
        this.description = description;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Area id(Long id) {
        this.id = id;
        return this;
    }

    public Area value(String value) {
        this.value = value;
        return this;
    }

    public Area description(String description) {
        this.description = description;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", value='" + getValue() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }


}
