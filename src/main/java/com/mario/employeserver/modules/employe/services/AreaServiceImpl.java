package com.mario.employeserver.modules.employe.services;

import com.mario.employeserver.modules.employe.data.Area;
import com.mario.employeserver.modules.employe.repositories.AreaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This class implement area service and defined some method to handler business
 * logic for area
 *
 * @author Mario Jurado
 * @version 1.0
 * @since 2020-12-04
 */

@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    AreaRepository areaRepository;

    /**
     * This method let to retrieble all record 
     * of area table 
     * @return Iterable<Area>, list records of area table.
   */
    @Override
    public Iterable<Area> getAllAreas() {
        return areaRepository.findAll();
    }
    
}
