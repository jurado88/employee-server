package com.mario.employeserver.modules.employe.services;

import com.mario.employeserver.modules.employe.data.IdType;

/**
* This interface defined to behavior transations on IdType object.
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/

public interface IdTypeService {
    
    Iterable<IdType> getAllIdsType();
}
