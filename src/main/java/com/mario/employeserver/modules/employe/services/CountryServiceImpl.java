package com.mario.employeserver.modules.employe.services;

import com.mario.employeserver.modules.employe.data.Country;
import com.mario.employeserver.modules.employe.repositories.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* This class implement country service and defined
* some method to handler business logic for country
*
* @author  Mario Jurado
* @version 1.0
* @since   2020-12-04
*/
 
@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryRepository countryRepository;

    /**
     * This method let to retrieble all record 
     * of country table 
     * @return Iterable<Country>, list records of country table.
   */
    @Override
    public Iterable<Country> getAllCountries() {
        return countryRepository.findAll();
    }
    
}
