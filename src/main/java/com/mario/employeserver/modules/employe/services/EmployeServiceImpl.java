package com.mario.employeserver.modules.employe.services;

import java.util.Date;
import java.util.List;

import com.mario.employeserver.modules.employe.data.Employe;
import com.mario.employeserver.modules.employe.repositories.EmployeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * This class implement country service and defined some method to handler
 * business logic for country
 *
 * @author Mario Jurado
 * @version 1.0
 * @since 2020-12-04
 */

@Service
public class EmployeServiceImpl implements EmployeService {

    @Autowired
    EmployeRepository employeRepository;

    /**
     * This method allow insert an employe 
     * @param Employe, employee to insert 
     * @return Employe created
    */
    @Override
    public Employe insertEmploye(Employe employe) {
        employe.setEnabled(true);
        employe.setCreationDate(new Date());
        employe.setFirstName(employe.getFirstName().toUpperCase());
        employe.setOtherName(employe.getOtherName().toUpperCase());
        employe.setFirstSurname(employe.getFirstSurname().toUpperCase());
        employe.setSecondSurname(employe.getSecondSurname().toUpperCase());
        employe.setEmail(
                this.generateEmail(employe.getFirstName(), employe.getFirstSurname(), employe.getCountry().getId()));
        return employeRepository.save(employe);
    }

    /**
     * This method return all employes
     * @return Iterable<Employe> 
    */
    @Override
    public Iterable<Employe> getAllEmploye() {
        return employeRepository.findAll();
    }

    /**
     * This method allow delete an employe 
     * @param Employe, employee to delete 
    */
    @Override
    public void deleteEmployee(Employe employe) {
        employeRepository.delete(employe);
    }

    /**
     * This method find an employe by identification
     * @param identificarion, identification use to find employe 
     * @return List<Employe>
    */
    @Override
    public List<Employe> findByIdentification(String identification) {
        return employeRepository.findByIdentification(identification);
    }

    /**
     * This method returned employes pageables
     * @param Pageable, configuration to pageable response 
     * @return Page<Employe> 
    */
    @Override
    public Page<Employe> findAll(Pageable pageable) {
        return employeRepository.findAll(pageable);
    }

    /**
     * This method returned employes pageables and search by criterion
     * @param Pageable, configuration to pageable response
     * @param criterion, criterion to find employes 
     * @return Page<Employe> 
    */
    @Override
    public Page<Employe> findAll(Pageable pageable, String criterion) {
        return employeRepository.findByCriterion(criterion, pageable);
    }

    /**
     * This method update a employe
     * @param employe, record to update
     * @return Employe 
    */
    @Override
    public Employe updateEmploye(Employe employe) {
        return employeRepository.save(employe);
    }

    /**
     * This method generated to email an employee
     * @param firsname
     * @param firsSurname
     * @param countryCode 
     * @return String with email generated to assing employe 
    */
    private String generateEmail(String firsName, String firstSurname, Long countryCode) {

        String firstPart = firsName.replaceAll("\\s+", "").toLowerCase();
        String secondPart = firstSurname.replaceAll("\\s+", "").toLowerCase();
        String thirdPart;
        if (countryCode == 1) {
            thirdPart = "@cidenet.com.co";
        } else {
            thirdPart = "@cidenet.com.us";
        }
        Integer sequence = this.generateSequence(firstPart + "." + secondPart, countryCode);
        if (sequence == 0) {
            return firstPart + "." + secondPart + thirdPart;
        } else {
            return firstPart + "." + secondPart + "." + sequence.toString() + thirdPart;
        }
    }

    /**
     * This method generated the sequence to assing the email
     * @param searchEmail email to generated sequence
     * @param idCountry country employe
     * @return Integer with sequence 
    */
    private Integer generateSequence(String searchEmail, Long idCountry) {
        String lastEmail = employeRepository.findLastEmail(searchEmail, idCountry);
        if (lastEmail == null) {
            return 0;
        }
        try {
            String emailSequence = lastEmail.split("@")[0];
            Integer sequence = Integer.parseInt(emailSequence.split("\\.")[2]);
            return sequence + 1;
        } catch (Exception ex) {
            return 1;
        }
    }
    
}
